/*jslint node:true, unparam: true */
'use strict';

// npm install parse-appcache-manifest
var fs = require('fs'),
    crypto = require('crypto'),
    http = require('http'),
    Q = require('q'),
    request = require('request'),
    appcacheParser = require('parse-appcache-manifest'),
    SECTIONS = ['CACHE', 'NETWORK', 'FALLBACK'],
    Obj;

Obj = function (opts, cb) {
    var self = this;

    if (!opts.src) {
        throw new Error('A cache manifest file must be provided (opts.src)');
    }
    self.src = opts.src;

    if (!opts.app) {
        throw new Error('An HTTP server instance must be provided (opts.app)');
    }
    self.app = opts.app;
    // the path this module responds to. By default /manifest.appcache
    self.path = opts.path || '/manifest.appcache';

    self.timeout = opts.timeout && opts.timeout >= 1000 ? opts.timeout : 30000; //30sec
    self.intervalTtl = opts.ttl && opts.ttl > self.timeout ? opts.ttl : 300000; //5min

    // give the server some time to do it's stuff
    // dustjs/kraken return zero length pages if we call the server wile loading
    setTimeout(function () {
        self.runTasks(cb);
    }, 1000);
};

Obj.handler = function (opts) {
    var obj = new Obj(opts);
    return obj.handler();
};

// the HTTP server instance. Usually express
Obj.prototype.app = null;
// a local "server" using the app
Obj.prototype.localServer = null;
//cache manifest src file. Must be the full path to the file
Obj.prototype.src = null;
// path. defaults to /manifest.appcache
Obj.prototype.path = null;
// contains the parsed src file.
Obj.prototype.data = null;
// the generated/parsed cachemanifest file 
Obj.prototype.output = null;
// timer used to re-run tasks
Obj.prototype.timer = null;
// interval for re-running tasks
Obj.prototype.intervalTtl = null;
// timeout for the request that checks the etag
Obj.prototype.timeout = null;

Obj.prototype.loadFile = function (filePath) {
    var deferred = Q.defer();

    fs.readFile(filePath, {
        encoding: 'utf8'
    }, function (err, file) {
        if (err) {
            deferred.reject(err);
        }
        deferred.resolve(file);
    });

    return deferred.promise;
};

Obj.prototype.parseFile = function (file) {
    var self = this,
        promises = [],
        data;

    data = appcacheParser(file);
    self.data = {};

    SECTIONS.forEach(function (section) {
        var sectionData = data[section.toLowerCase()];
        if (sectionData) {
            // FALLBACK gets parsed into an object while the other go into arrays
            if (section === 'FALLBACK') {
                self.data[section] = Object.keys(sectionData).map(function (fallbackKey) {
                    var item = {};

                    item.src = fallbackKey + ' ' + sectionData[fallbackKey];
                    item.href = sectionData[fallbackKey];
                    item.etag = null;

                    promises.push(self.getEtag(item));

                    return item;
                });
            } else {
                self.data[section] = sectionData.map(function (src) {
                    var item = {};

                    item.src = src;
                    item.href = src;
                    item.etag = null;
                    if (section === 'CACHE') {
                        promises.push(self.getEtag(item));
                    }

                    return item;
                });
            }
        }
    });
    return Q.all(promises);
};

Obj.prototype.generateOutput = function () {
    //todo: check the actual cache manifest file. Timestamp?
    var self = this,
        appcacheOutputHeader = '',
        appcacheOutputChecksum = '',
        appcacheOutputSections = '',
        output;

    appcacheOutputHeader += 'CACHE MANIFEST' + "\r\n";

    SECTIONS.forEach(function (section) {
        var sectionData = self.data[section];
        if (sectionData) {
            appcacheOutputSections += "\r\n" + section + ':' + "\r\n";
            sectionData.forEach(function (item) {
                if (item.src && item.etag) {
                    appcacheOutputChecksum += item.etag;
                }
                appcacheOutputSections += item.src + "\r\n";
            });
        }
    });

    appcacheOutputChecksum = '#checksum: ' + crypto.createHash('md5').update(appcacheOutputChecksum).digest('hex') + "\r\n";

    output = appcacheOutputHeader + appcacheOutputChecksum + appcacheOutputSections;

    self.output = output;
};

Obj.prototype.runTasks = function (cb) {
    var self = this,
        done;

    clearInterval(self.timer);

    done = function (err) {
        clearInterval(self.timer);
        self.timer = setTimeout(self.runTasks.bind(self), self.intervalTtl);
        if (cb) {
            cb(err);
        }

        done = function () {}; //just in case both "catch" and "done" get triggered
    };

    self.loadFile(self.src)
        .then(self.parseFile.bind(self))
        .then(self.generateOutput.bind(self))['catch'](done).done(done);
};

Obj.prototype.handler = function () {
    var self = this;

    return function manifestMiddleware(req, res, next) {
        if (req.path !== self.path) {
            return next();
        }

        if (self.output) {
            res.status(200).set('Content-Type', 'text/cache-manifest');
            res.send(self.output);
        } else {
            res.sendStatus(500);
        }
    };
};

Obj.prototype.getEtag = function (item) {
    var self = this,
        deferred = Q.defer(),
        url = '',
        localserver,
        localPort;
    if (!item.href) {
        deferred.reject(new Error('The item should have an href property'));
    } else {
        if (/^https?:\/\//.test(item.href)) {
            url = item.href;
        } else {
            localserver = self.getLocalServer();
            localPort = localserver.address().port;
            url = 'http://localhost:' + localPort + item.href;
        }

        request({
            url: url,
            method: 'GET', //todo: change to HEAD as soon as express allows it
            timeout: self.timeout
        }, function (error, response, body) {
            if (error || response.statusCode !== 200) {
                // just because we can't access the file doesn't mean we should remove it
                // from the manifest. We'll just add a comment to the output file
                item.etag = 'unable to access'; //default value. must be truthy
                item.src = '# unable to check ETag or content \r\n' + item.src;
            } else {
                if (response.headers.etag) {
                    item.etag = response.headers.etag;
                } else {
                    item.etag = crypto.createHash('md5').update(body).digest('hex');
                }
            }
            deferred.resolve();
        });
    }

    return deferred.promise;
};

Obj.prototype.getLocalServer = function () {
    var self = this;
    if (!self.localServer) {
        self.localServer = http.createServer(self.app).listen(0);

    }
    return self.localServer;
};

module.exports = Obj;