# Parses and serves a cache manifest file

This module enables you to serve a standard cache manifest text file with an express server. The module checks when any of the resources change, using either ETags or the content body, and bumps the cache manifest file accordingly. The module keeps checking the resources on a fixed interval.

## usage

Basic usage:
    
    var path = require('path'),
        express = require('express'),
        ManifestMiddleware = require('manifest-middleware'),
        manifestFile = path.join(__dirname, 'manifest.appcache'),
        app;

    app = express();
    app.use(ManifestMiddleware.handler({
        app: app,
        src: manifestFile
    }));
    app.listen(8000);//or any other port you like

You can now serve your manifest file from 'http://localhost:8000/manifest.appcache'.

More information about cache manifest: http://en.wikipedia.org/wiki/Cache_manifest_in_HTML5

## options

### src
the full path (on disk) for the cache manifest file. Eg. "/usr/share/sites/mySite.com/public/manifest.appcache".
You can calso use path.join and __dirname to get the full path

### app
An express (http://expressjs.com/) instance. It may work with other servers but it hasn't been tested. Feel free to do so and give feedback.

### path (optional)
The public path that will serve the manifest. By default it's /manifest.appcache

### timeout (optional)
The max time (in ms) the request can take while checking any of the resources. By default is 30000 (30 sec).

### ttl (optional)
Inteval (in ms) between checks. By default 300000 (5 min).

