/*jslint node:true, unparam: true, nomen: true */
/*global describe, it, before */
'use strict';

var express = require('express'),
    supertest = require('supertest'),
    AppcacheMiddleware = require('../'),
    appcacheParser = require('parse-appcache-manifest'),
    path = require('path'),
    fs = require('fs'),
    assert = require("assert");

describe('Manifest Middleware', function () {
    describe('Unit testing', function () {
        it('should throw error loading empty file path', function (done) {
            AppcacheMiddleware.prototype.loadFile.call(null, '')['catch'](function (err) {
                assert(err instanceof Error);
                done();
            });
        });

        it('should create an instance', function (done) {
            var app = express(),
                handler = AppcacheMiddleware.handler({
                    app: app,
                    src: path.join(__dirname, '../fixture/manifest.appcache')
                });

            app.use(handler);

            done();
        });

        it('should throw error, missing app', function (done) {
            assert.throws(function () {
                var app = express(),
                    handler = AppcacheMiddleware.handler({
                        src: path.join(__dirname, '../fixture/manifest.appcache')
                    });

                app.use(handler);
            });
            done();
        });

        it('should throw error, missing src', function (done) {
            assert.throws(function () {
                var app = express(),
                    handler = AppcacheMiddleware.handler({
                        app: app
                    });

                app.use(handler);
            });
            done();
        });

    });

    describe('Integration testing', function () {
        var app,
            middleware,
            responseText = '',
            testResponse = 'OK';

        before(function (done) {
            this.timeout(30000);
            app = express();

            middleware = new AppcacheMiddleware({
                app: app,
                src: path.join(__dirname, '../fixture/manifest.appcache'),
                path: '/manifest.appcache',
                timeout: 10000,
                ttl: 30000
            }, function (err) {
                app.use(middleware.handler());
                done(err);
            });

            // dummy routes
            app.get('/', function (req, res) {
                res.send('OK 1');
            });
            app.get('/test', function (req, res) {
                res.send(testResponse);
            });
            app.get('/offline/img', function (req, res) {
                res.send('OK 2');
            });
        });

        it('should return a valid cache file', function (done) {
            supertest(app)
                .get('/manifest.appcache')
                .expect('Content-Type', /text\/cache-manifest/)
                .expect(200)
                .end(function (err, res) {
                    var originalManifest, loadedManifest;

                    fs.readFile(path.join(__dirname, '../fixture/manifest.appcache'), {
                        encoding: 'utf8'
                    }, function (err, file) {
                        if (err) {
                            throw err;
                        }
                        responseText = res.text;

                        originalManifest = appcacheParser(file);
                        loadedManifest = appcacheParser(res.text);
                        //check if the files have the same resources
                        Object.keys(originalManifest.fallback).forEach(function (key) {
                            assert.equal(originalManifest.fallback[key], loadedManifest.fallback[key]);
                        });
                        ['cache', 'network'].forEach(function (section) {
                            originalManifest[section].forEach(function (val) {
                                var index = loadedManifest[section].indexOf(val);
                                assert(index > -1);
                            });
                        });

                        done();
                    });
                });
        });

        it('should return the same cache file', function (done) {
            supertest(app)
                .get('/manifest.appcache')
                .expect('Content-Type', /text\/cache-manifest/)
                .expect(200)
                .end(function (err, res) {
                    assert.equal(responseText, res.text);
                    done(err);
                });
        });

        it('should return a different cache file', function (done) {
            testResponse = 'KO';

            middleware.runTasks(function () {
                supertest(app)
                    .get('/manifest.appcache')
                    .expect('Content-Type', /text\/cache-manifest/)
                    .expect(200)
                    .end(function (err, res) {
                        assert.notEqual(responseText, res.text);
                        responseText = res.text;
                        done(err);
                    });
            });
        });
    });
});